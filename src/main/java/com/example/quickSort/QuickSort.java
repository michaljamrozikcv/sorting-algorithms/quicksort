
package com.example.quickSort;

import java.util.Random;

class QuickSort {
    /**
     * złożoność obliczeniowa: O(n*log n)
     * złożoność pamięciowa: Omega (1)
     */
    static void quickSort(int[] array, int leftIndex, int rightIndex) {
        if (leftIndex >= rightIndex) {
            return;
        }
        int pivot = choosePivot(array, leftIndex, rightIndex);
        int border = leftIndex;
        int i = leftIndex;
        //compare each value with chosen pivot
        //if value is lower than pivot, swap value with border's value and move border
        while (i < rightIndex) {
            if (array[i] < array[pivot]) {
                //no need to swap elements with the same index
                if (border != i) {
                    swapAtIndexes(array, i, border);
                }
                border++;
            }
            i++;
        }
        //swap pivot value with border's value
        //no need to swap elements with the same index
        if (array[border] > array[pivot]) {
            if (border != pivot) {
                swapAtIndexes(array, border, pivot);
            }
        }
        //recursion on two sub-arrays, element on border is already on correct place
        quickSort(array, 0, border - 1);
        quickSort(array, border + 1, rightIndex);
    }

    private static void swapAtIndexes(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

    /**
     * By choosing random pivot we will avoid worst case O(n^2) - when array is already sorted
     */
    private static int choosePivot(int[] array, int index1, int index2) {
        int pivot = (index1 + index2) / 2;
        //swap pivot value to last index
        swapAtIndexes(array, pivot, index2);
        return index2;
    }

    /**
     * Return array of random integers
     *
     * @param size   the number of values to generate
     * @param origin the origin (inclusive) of each random value
     * @param bound  the bound (exclusive) of each random value
     */
    static int[] createRandomArray(int size, int origin, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            array[i] = random.ints(1, origin, bound).findFirst().getAsInt();
        }
        return array;
    }
}
