package com.example.quickSort;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = QuickSort.createRandomArray(30, -30, 200);
        System.out.println("Array before sorting:");
        System.out.println(Arrays.toString(array));
        System.out.println("Sorted array:");
        QuickSort.quickSort(array, 0, array.length - 1);
        System.out.println(Arrays.toString(array));
    }
}
